# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 20:18:11 2016

@author: hari
@email: hbhrugubanda@hotmail.com

"""

import pandas as pd
import numpy as np
import sys

import matplotlib.pyplot as plt


showplots = False


#from sklearn.preprocessing import MinMaxScaler

# strips the "location" from the location column
def strip_keyword(x, col_name):
    return [int(a) for a in x.split(col_name) if a != ''][0]
    
# read in the csv
submission = False  

train = pd.read_csv('train.csv')
    
severity_type = pd.read_csv('severity_type.csv')
log_feature = pd.read_csv('log_feature.csv')
event_type = pd.read_csv('event_type.csv')


# sanitise data (remove words from columns)
train["location"] = train["location"].apply(strip_keyword, args=('location',))
severity_type["severity_type"] = severity_type["severity_type"].apply(strip_keyword, args=('severity_type',))
log_feature["log_feature"] = log_feature["log_feature"].apply(strip_keyword, args=('feature',))
event_type["event_type"] = event_type["event_type"].apply(strip_keyword, args=('event_type',))

# join everything together to make things easier
result = pd.merge(train, severity_type, on='id')
result = pd.merge(result, log_feature, on='id')
result = pd.merge(result, event_type, on='id')

# rearrange the columns so that fault_severity is last
cols = result.columns.tolist()
cols = cols[:2] + cols[3:] + [cols[2]]
result = result[cols]

# Columns
# ['id', 'location', 'severity_type', 'log_feature', 'volume', 'event_type', 'fault_severity']


# plot histograms for train, severity_type, log_feature and event type on the same plot
# this is all plotted before the merge
fig = plt.figure(1)
if not submission:
    plt.subplot(231)
    plt.hist(train["fault_severity"])
    plt.title('Fault Severity')
    plt.grid(True)

plt.subplot(232)
plt.hist(severity_type["severity_type"])
plt.title('Severity Type')
plt.grid(True)

plt.subplot(233)
plt.hist(log_feature["log_feature"])
plt.title('Log Feature - log_feature')
plt.grid(True)

plt.subplot(234)
plt.hist(log_feature["volume"])
plt.title('Log Feature - volume')
plt.grid(True)

plt.subplot(235)
plt.hist(event_type["event_type"])
plt.title('Event Type')
plt.grid(True)


plt.subplot(236)
plt.hist(train["location"])
plt.title('Location')
plt.grid(True)
plt.subplots_adjust(hspace=0.3, wspace=0.3)

if showplots:
    plt.show()

# Plot scatter graphs of everything vs the classification
# This is plotted after the merge
fig = plt.figure(2)

gen_color = lambda : np.random.rand(3,)

if not submission:
    plt.subplot(231)
    plt.scatter(result["location"], result["fault_severity"])
    plt.title('Location')
    plt.grid(True)
    
    plt.subplot(232)
    plt.scatter(result["severity_type"], result["fault_severity"], c=gen_color())
    plt.title('Severity Type')
    plt.grid(True)
    
    plt.subplot(233)
    plt.scatter(result["log_feature"], result["fault_severity"], c=gen_color())
    plt.title('Log Feature - log_feature')
    plt.grid(True)
    
    plt.subplot(234)
    plt.scatter(result["volume"], result["fault_severity"], c=gen_color())
    plt.title('Log Feature - volume')
    plt.grid(True)
    
    plt.subplot(235)
    plt.scatter(result["event_type"], result["fault_severity"], c=gen_color())
    plt.title('Event Type')
    plt.grid(True)
    
    plt.subplots_adjust(hspace=0.3, wspace=0.3)
    
    if showplots:
        plt.show()

#print(result.head(10))

# the volume of entries definitely increases for certain locations. Maybe we should try to
# remove infrequent and too frequent log_features
# plt.scatter(result['location'], result['volume'])

plt.scatter(result['log_feature'], result['volume'])
plt.scatter(result['log_feature'], result['event_type'])

# TODO: Think about the problem properly, what would influence outages?
# to me, traffic is probably dependent on location, and volume 
# For the log features, group by id. Use number of log features and location.

# TODO: an use crosstab to give more information about the frequency of results
#pd.crosstab(result['event_type'], result['fault_severity'])

search = True


# TODO: CHoose a good machine learning algorithm based on the above
from sklearn import linear_model, cross_validation
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline
if search:
    from sklearn.grid_search import GridSearchCV

logistic = linear_model.LogisticRegression(C=1.0, penalty='l1', tol=1e-3)

pipe = Pipeline(steps=[
        ('min/max scaler', MinMaxScaler(feature_range=(-1.0, 1.0))),
        ('logistic', logistic)
        ])

features = cols[1:-1]
#make more features

#for feature in features:
#    result[feature+'2'] = result[feature].apply(np.square)


X  = result[features]
y = result[cols[-1]]

X_train, X_test, y_train, y_test = cross_validation.train_test_split(X, y, test_size=0.3, random_state=0)

print(X.head(10))
if search:
    Cs = np.logspace(-4, 4, 3)
    estimator = GridSearchCV(pipe, 
                             dict(logistic__C=Cs))  
    estimator.fit(X_train, y_train)
    print(estimator.score(X_test, y_test))
    print(estimator.best_estimator_)
    sys.exit()



if not submission:
    clf = pipe.fit(X_train, y_train)
    print(clf.score(X_test, y_test))
    #print(clf.score(X_train, y_train))
    sys.exit()
else:
    clf = pipe.fit(X, y)





##### Continue if making a submission
## Predicting part on test set
test = pd.read_csv('test.csv')

# sanitise data (remove words from columns)
test["location"] = test["location"].apply(strip_keyword, args=('location',))
#severity_type["severity_type"] = severity_type["severity_type"].apply(strip_keyword, args=('severity_type',))
#log_feature["log_feature"] = log_feature["log_feature"].apply(strip_keyword, args=('feature',))
#event_type["event_type"] = event_type["event_type"].apply(strip_keyword, args=('event_type',))


# join everything together to make things easier
test_result = pd.merge(test, severity_type, on='id')
test_result = pd.merge(test_result, log_feature, on='id')
test_result = pd.merge(test_result, event_type, on='id')

# rearrange the columns so that fault_severity is last
#cols = test_result.columns.tolist()
#cols = cols[:2] + cols[3:] + [cols[2]]
#test_result = test_result[cols[:-1]]

#predicting bit
print(test_result.head(10))
predictions = clf.predict(test_result[cols[1:-1]])

test_result['fault_severity'] = predictions

submission = test_result[['id','fault_severity']]
submission = submission.drop_duplicates(subset='id')

predict_cols = ['predict_0', 'predict_1', 'predict_2']

for col in predict_cols:
    submission[col] = 0

submission_cols = ['id']
submission_cols.extend(predict_cols)

submission = submission.as_matrix()

submission_csv = []

for row in submission:
    severity_num = row[1]
    row[2+severity_num] = 1
    
    submission_csv.append(np.concatenate(([row[0]], row[2:])))


print(submission_cols)
submission_csv = np.array(submission_csv)
np.savetxt("submission.csv", submission_csv, delimiter=",", fmt='%d', header=','.join(submission_cols))






# TODO: Optimise machine learning algorithm



#print(result.head(10))

